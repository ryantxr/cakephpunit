# README #

This is PHPUnit 3.7 with supporting files for it to work with CakePHP 2.x.

### What is this repository for? ###

* PHPUnit for CakePHP 2.x
* 1.0

### How do I get set up? ###

* Create a directory /usr/share/php
* Put all the files from this repo into /usr/share/php
* Add /usr/share/php to include_path in php.ini.
